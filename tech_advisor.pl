:-module(tech_advisor, [main/0, clean/0, print_feat/0, tell_mobile/0, tell_frontend/0, tell_backend/0, 	known/3, isTrue/2, isTrue/1]).

:- use_module(library(pengines)).

%% :-dynamic([has_feature/2]).
:-dynamic([known/3]).



%%%%%%%% hipotezy

%TODO: frontend should use it too
mobile_should_use(responsive_website) :- 
	%% mobile_multiplatform,
	%% isTrue(rapid_development),
	not(isTrue(uses_sensors)),
	not(isTrue(speed_is_critical)).

	% TODO: lang : javascript
mobile_should_use(phone_gap) :-
	mobile_multiplatform,
	isTrue(rapid_development),
	not(isTrue(speed_is_critical)).
	%% !.
	
mobile_should_use(native_app) :-
	mobile_multiplatform,
	isTrue(design_is_important),
	isTrue(resources, big).
	%% !.

mobile_should_use(native_app) :-
	not(mobile_multiplatform).

frontend_should_use(bootstrap) :-
	responsive_design.

frontend_should_use(bootstrap) :-
	isTrue(design_is_important),
	no_language_contraindication(javascript).



frontend_should_use(gwt) :-
	not(isTrue(design_is_important)),
	no_language_contraindication(java).

frontend_should_use(mvc_framework) :-
	%% isTrue(design_is_important),
	no_language_contraindication(javascript).

backend_should_use(paaS) :-
	not(resources(big)),
	isTrue(easy_maintainance),
	isTrue(data_can_be_handed_to_3rd_party).



backend_should_use(apache_storm) :-
	big_data,
	no_language_contraindication(java),
	not(isTrue(realtime_processing)).
	

backend_should_use(apache_hadoop) :-
	big_data,
	no_language_contraindication(java).
	


backend_should_use(akka) :-
	distributed_architecture,
	isTrue(scalability),
	no_language_contraindication(scala).


backend_should_use(erlang) :-
	isTrue(scalability),
	no_language_contraindication(erlang).

backend_should_use(erlang) :-
	distributed_architecture,
	no_language_contraindication(erlang).


backend_should_use(play_framework) :-
	isTrue(legacy_system, java),
	no_language_contraindication(scala).


backend_should_use(python) :-
	true.

multivalue(mobile_platform).
multivalue(desktop_platform).
multivalue(language).

responsive_design :- 
	multiplatform.

responsive_design :-
	mobile_should_use(responsive_website).

%% responsive_design :-
%% 	mobile_multiplatform.

mobile_multiplatform :-
	more_than_one([ios, android, windows_phone, blackberry], mobile_platform).

multiplatform :-
	more_than_one([windows, linux, mac], desktop_platform).

resources(big) :-
	isTrue(capital, big);
	isTrue(developers_capacity, big).

distributed_architecture :-
	isTrue(userbase_size, big),
	isTrue(high_availability).

big_data :-
	isTrue(has_recommendation_system),
	isTrue(userbase_size, big).

big_data :-
	isTrue(user_activity_analysis),
	isTrue(userbase_size, big).

starting_from_scratch :-
	findall(Lang, isTrue(legacy_system, Lang), L),
	length(L, Len),
	Len == 0.


no_language_contraindication(Language) :-
	starting_from_scratch, 
	include(isTrue(language), [java, ruby, javascript, erlang, scala], Preferences),
	member(Language, Preferences).

no_language_contraindication(Language) :-
	findall(Lang, isTrue(legacy_system, Lang), L),
	member(Language, L).


more_than_one(List, QueryType) :-
	include(isTrue(QueryType), List, Preferences),
	length(Preferences, Platforms),
	Platforms >= 2.

isTrue(Query) :-
	isTrue(general, Query).

isTrue(Tag, Query) :-
	known(Tag, Query, true),
	!.
isTrue(Tag, Query) :-
	known(Tag, Query, false),
	!, false.

isTrue(Tag, Query) :-
	output([Tag, Query]),
	input("true/false?", Answer),
	assertz(known(Tag, Query, Answer)),
	Answer.

%TODO: obsluga not multivalue -> znalezienie jakiejkolwiek wartosci		


clean :-
	output("Cleaning database"), 
	retractall(known(_, _, _)).
   	


tell_mobile :-
	mobile_should_use(Tech),
	format('Use technology ~w for mobile\n', [Tech]).
	%% output([Tech]). 

tell_frontend :-
	frontend_should_use(Tech),
	format('Use technology ~w for frontend\n', [Tech]).
	%% output([Tech]). 

tell_backend :-
	backend_should_use(Tech),
	format('Use technology ~w for backend\n', [Tech]).	
	%% output([Tech]). 


main :-
	clean,
	tell_mobile,
	tell_frontend,
	tell_backend,
	!.

main :-
	output("This project is a death march"). % format('This project is a death march').

	
output(Msg) :-
	writeln(Msg).
	%% pengine_output(Msg).
input(Msg, Answer) :-
	writeln(Msg),
	read(Answer).
	%% pengine_input(Msg, Answer).

print_feat :-
    writeln('Has features:'),
    forall(known(Answer, V, A), format("~w - ~w - ~w\n", [Answer, V, A])). 

