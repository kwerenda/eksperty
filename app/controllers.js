'use strict';

var techControllers = angular.module('techControllers', []);

function MainCtrl($scope, RestService) {
    $scope.output = ['Welcome!'];

    RestService.registerOutCallback(function (output) {
        $scope.$apply(function () {
            $scope.output.push(output);
        });
    });

    $scope.connect = function () {
        RestService.connect();
    };

    $scope.respond = function () {
        if ($scope.query) {
            RestService.respond($scope.query);
            RestService.pull();
        } else {
            $scope.output.push('Provide a query!');
        }
    };

    $scope.pull = function () {
        RestService.pull();
    };
}

techControllers.controller('MainCtrl', MainCtrl);
