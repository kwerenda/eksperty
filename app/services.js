'use strict';

var techServices = angular.module('techServices', []);

function RestService() {
    var that = this;

    var host = 'http://localhost:3030/pengine';
    var appName = 'tech_advisor';    // back-end's application's name (Prolog)
    var initPred = 'main';              // back-end's initialization predicate

    var pengine;
    var sessionId;
    var outCallback;

    this.registerOutCallback = function (outputCallback) {
        outCallback = outputCallback;
    };

    this.connect = function () {
        sessionId = null;

        pengine = new Pengine({
            server: host,
            application: appName,
            ask: initPred,
            onsuccess: function () {
                console.log("[Success]");
            },
            onfailure: function () {
                console.log("[Failure]");
            },
            onstop: function () {
                console.log("[Stopped]");
            },
            onabort: function () {
                console.log("[Aborted]");
            },
            onerror: function () {
                outCallback(this.data);
            },
            onoutput: function () {
                if (!sessionId) sessionId = this.id;
                that.handleOutput(this);
            }
        });
    };

    this.handleOutput = function (output) {
        var data = output.data;

        if (data.args) {
            if (data.args.length == 2) {
                outCallback(data.args[0] + ': ' + data.args[1]);
            } else {
                outCallback('TIP: ' + data.args[0]);
            }
        } else if (data) {
            outCallback(output.data);
        }
    };

    this.respond = function (input) {
        pengine.respond(input);
    };

    this.pull = function () {
        pengine.pull_response(sessionId);
    };
}

techServices.service('RestService', RestService);
